/**
 * @brief Extern `vltData_s` from `main.c`
 */
extern struct vltData_s vltData;

/**
 * @name Bit representation of commands:
 * @{
 */

/**
 * @def COMMAND_TIMESTAMP
 * @brief Timestamp request command.
 */
#define COMMAND_TIMESTAMP (uint8_t)0x01
/**
 * @def COMMAND_OTHER
 * @brief Other command.
 */
#define COMMAND_OTHER (uint8_t)0x02
