/*******************************************************************************
 * Copyright (c) 2015 Thomas Telkamp and Matthijs Kooijman
 *
 * Permission is hereby granted, free of charge, to anyone
 * obtaining a copy of this document and accompanying files,
 * to do whatever they want with them without any restriction,
 * including, but not limited to, copying, modification and redistribution.
 * NO WARRANTY OF ANY KIND IS PROVIDED.
 *
 * This example sends a valid LoRaWAN packet with payload "Hello,
 * world!", using frequency and encryption settings matching those of
 * the KPN Lora Test network.
 *
 * This doesn't use OTAA (Over-the-air activation), where where a DevEUI and
 * application key is configured, which are used in an over-the-air
 * activation procedure where a DevAddr and session keys are
 * assigned/generated for use with all further communication.
 *
 * Note: LoRaWAN per sub-band duty-cycle limitation is enforced (1% in
 * g1, 0.1% in g2), but not the KPN Lora policy (which is probably
 * violated by this sketch when left running for longer)!
 *
 * Do not forget to define the radio type correctly in config.h.
 *
 *******************************************************************************/

#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>

#include <lmic.h>
#include <hal/hal.h>
#include <string>
#include <raspi/RasPi.h>
#include <vlt/data.h>
#include "kpnloratest.h"

/*!
 * LoRaWAN Adaptative Data Rate
 */
#define LORAWAN_ADR_ON                              1
#define LORAWAN_ADR_OFF                             0

/*!
 * LoRaWAN confirmed messages
 */
#define LORAWAN_CONFIRMED_MSG_ON                    1
#define LORAWAN_CONFIRMED_MSG_OFF                   0


struct vltData_s vltData;
/* USER CODE END PV */

// LoRaWAN NwkSKey, network session key
// This is provided by KPN network initially.
//static const PROGMEM u1_t NWKSKEY[16] = { 0x08, 0xd5, 0x16, 0xb2, 0xc6, 0x72, 0xfa, 0x7c, 0xd0, 0xf6, 0x18, 0xde, 0x17, 0x96, 0x95, 0xd7 };
static const PROGMEM u1_t NWKSKEY[16] = { 0xf9, 0xf9, 0xc6, 0xce, 0xf3, 0xd6, 0x18, 0xb9, 0xea, 0xc8, 0xe2, 0xec, 0x87, 0x5d, 0x69, 0x07};


// LoRaWAN AppSKey, application session key
// This is provided by KPN network initially.
//static const u1_t PROGMEM APPSKEY[16] = { 0x11, 0xb4, 0xdf, 0x6f, 0xa6, 0xab, 0xa2, 0x2e, 0x29, 0x2b, 0x9f, 0xbe, 0xb8, 0xd6, 0x9f, 0x4d };
static const u1_t PROGMEM APPSKEY[16] = {0xdb, 0xf2, 0x2f, 0x35, 0x0b, 0x69, 0xa8, 0xfd, 0x00, 0x69, 0xa2, 0x0b, 0x09, 0x03, 0x37, 0x46 };

// LoRaWAN end-device address (DevAddr)
// This is provided by KPN network initially.
//static const u4_t DEVADDR = 0x14007B4B ; // <-- Change this address for every node!

static const u4_t DEVADDR = 0x1420602B;

// LoRaWAN AppSKey, application session key
// Use this key to get your data decrypted by The Things Network
static const u1_t PROGMEM ARTKEY[16] = { 0x11, 0xb4, 0xdf, 0x6f, 0xa6, 0xab, 0xa2, 0x2e, 0x29, 0x2b, 0x9f, 0xbe, 0xb8, 0xd6, 0x9f, 0x4d };

// LoRaWAN NwkSKey, network session key
// Use this key for The Things Network
static const u1_t PROGMEM DEVKEY[16] = { 0x08, 0xd5, 0x16, 0xb2, 0xc6, 0x72, 0xfa, 0x7c, 0xd0, 0xf6, 0x18, 0xde, 0x17, 0x96, 0x95, 0xd7 };


// These callbacks are only used in over-the-air activation, so they are
// left empty here (we cannot leave them out completely unless
// DISABLE_JOIN is set in config.h, otherwise the linker will complain).
// provide application router ID (8 bytes, LSBF)
void os_getArtEui (u1_t* buf) {}

// provide device ID (8 bytes, LSBF)
void os_getDevEui (u1_t* buf) {}

// provide device key (16 bytes)
void os_getDevKey (u1_t* buf) {}

static uint8_t mydata[] = "Raspi LMIC!";
static osjob_t sendjob;

// Schedule TX every this many seconds (might become longer due to duty)
// cycle limitations).
const unsigned TX_INTERVAL = 120;

//Flag for Ctrl-C
volatile sig_atomic_t force_exit = 0;

// LoRasPi board
// see https://github.com/hallard/LoRasPI
//#define RF_LED_PIN RPI_V2_GPIO_P1_16 // Led on GPIO23 so P1 connector pin #16
#define RF_CS_PIN  RPI_V2_GPIO_P1_24 // Slave Select on CE0 so P1 connector pin #24
#define RF_IRQ_PIN RPI_V2_GPIO_P1_22 // IRQ on GPIO25 so P1 connector pin #22
#define RF_RST_PIN RPI_V2_GPIO_P1_15 // RST on GPIO22 so P1 connector pin #15

// Raspberri PI Lora Gateway for multiple modules
// see https://github.com/hallard/RPI-Lora-Gateway
// Module 1 on board RFM95 868 MHz (example)
//#define RF_LED_PIN RPI_V2_GPIO_P1_07 // Led on GPIO4 so P1 connector pin #7
//#define RF_CS_PIN  RPI_V2_GPIO_P1_24 // Slave Select on CE0 so P1 connector pin #24
//#define RF_IRQ_PIN RPI_V2_GPIO_P1_22 // IRQ on GPIO25 so P1 connector pin #22
//#define RF_RST_PIN RPI_V2_GPIO_P1_29 // Reset on GPIO5 so P1 connector pin #29


// Dragino Raspberry PI hat (no onboard led)
// see https://github.com/dragino/Lora
//#define RF_CS_PIN  RPI_V2_GPIO_P1_22 // Slave Select on GPIO25 so P1 connector pin #22
//#define RF_IRQ_PIN RPI_V2_GPIO_P1_07 // IRQ on GPIO4 so P1 connector pin #7
//#define RF_RST_PIN RPI_V2_GPIO_P1_11 // Reset on GPIO17 so P1 connector pin #11



// Pin mapping
const lmic_pinmap lmic_pins = {
    .nss  = RF_CS_PIN,
    .rxtx = LMIC_UNUSED_PIN,
    .rst  = RF_RST_PIN,
    .dio  = {LMIC_UNUSED_PIN, LMIC_UNUSED_PIN, LMIC_UNUSED_PIN},
};

#ifndef RF_LED_PIN
#define RF_LED_PIN NOT_A_PIN
#endif


static void processRxFrame(void)
{
    uint8_t downlinkPayload[64] =
    { 0 };


    printf("DOWNLINK OCCURRED:\n"); printf("Entire frame: ");
    //debug_buf (LMIC.frame+LMIC.dataBeg, LMIC.dataLen);


    for (uint8_t j = 0, i = LMIC.dataBeg; i < LMIC.dataBeg + LMIC.dataLen;
            ++i, ++j)
    {
        downlinkPayload[j] = (uint8_t) LMIC.frame[i];
    }


    printf("PAYLOAD: ");
    //debug_buf (downlinkPayload, LMIC.dataLen);
    printf("Frame Version: ");
    //debug_val( NULL, downlinkPayload[0]);
    //printf("\n");



    union uint16_u downlinkAnswer;
    downlinkAnswer.val = 0;
    for (uint8_t i = 0; i < 2; ++i)
        downlinkAnswer.bin[i] = downlinkPayload[i + 1];

    printf("Answer for uplink message no.: ");
    //debug_val(NULL, downlinkAnswer.val);
        printf("\n");
    printf("Downlink commands:");
    debug_showCommands(downlinkPayload[3]);
    printf("\n");

    executeCommands(downlinkPayload[3], &downlinkPayload[4], &vltData);
    deletePositionFromRequestList(&vltData, downlinkAnswer.val);


}


void do_send(osjob_t* j) {
    char strTime[16];

    getSystemTime(strTime , sizeof(strTime));
    printf("%s: ", strTime);

    uint8_t payloadLen = prepareVltFrame(&vltData);
    /*
     * after-send state clear
     */
    vltData.state = 0x00;


    // Check if there is not a current TX/RX job running
    if (LMIC.opmode & OP_TXRXPEND) {
        printf("OP_TXRXPEND, not sending\n");
    } else {
        //digitalWrite(RF_LED_PIN, HIGH);
        // Prepare upstream data transmission at the next possible time.
            /*
     * Send packet and save its time
     */
    LMIC_setTxData2(10, LMIC.frame, payloadLen, LORAWAN_CONFIRMED_MSG_ON); //XXX should be ON
        //LMIC_setTxData2(1, mydata, sizeof(mydata)-1, 0);
        printf("Packet queued\n");
    }
    // Next TX is scheduled after TX_COMPLETE event.
}

void onEvent (ev_t ev) {
    printf("onEvent \n");
    char strTime[16];

    getSystemTime(strTime , sizeof(strTime));
    printf("%s: ", strTime);

    switch(ev) {
        case EV_SCAN_TIMEOUT:
            printf("EV_SCAN_TIMEOUT\n");
        break;
        case EV_BEACON_FOUND:
            printf("EV_BEACON_FOUND\n");
        break;
        case EV_BEACON_MISSED:
            printf("EV_BEACON_MISSED\n");
        break;
        case EV_BEACON_TRACKED:
            printf("EV_BEACON_TRACKED\n");
        break;
        case EV_JOINING:
            printf("EV_JOINING\n");
        break;
        case EV_JOINED:
            printf("EV_JOINED\n");

            // Disable link check validation (automatically enabled
            // during join, but not supported by TTN at this time).
            LMIC_setLinkCheckMode(0);
        break;
        case EV_RFU1:
            printf("EV_RFU1\n");
        break;
        case EV_JOIN_FAILED:
            printf("EV_JOIN_FAILED\n");
        break;
        case EV_REJOIN_FAILED:
            printf("EV_REJOIN_FAILED\n");
        break;
        case EV_TXCOMPLETE:
            printf("EV_TXCOMPLETE (includes waiting for RX windows)\n");
            if (LMIC.txrxFlags & TXRX_ACK)
              printf("%s Received ack\n", strTime);
            if (LMIC.dataLen) {
              printf("%s Received %d bytes of payload\n", strTime, LMIC.dataLen);
              processRxFrame();


            }

            // Schedule next transmission
            os_setTimedCallback(&sendjob, os_getTime()+sec2osticks(TX_INTERVAL), do_send);
        break;
        case EV_LOST_TSYNC:
            printf("EV_LOST_TSYNC\n");
        break;
        case EV_RESET:
            printf("EV_RESET\n");
        break;
        case EV_RXCOMPLETE:
            // data received in ping slot
            printf("EV_RXCOMPLETE\n");
        break;
        case EV_LINK_DEAD:
            printf("EV_LINK_DEAD\n");
        break;
        case EV_LINK_ALIVE:
            printf("EV_LINK_ALIVE\n");
        break;
        default:
            printf("Unknown event\n");
        break;
    }
}

/* ======================================================================
Function: sig_handler
Purpose : Intercept CTRL-C keyboard to close application
Input   : signal received
Output  : -
Comments: -
====================================================================== */
void sig_handler(int sig)
{
  printf("\nBreak received, exiting!\n");
  force_exit=true;
}

/* ======================================================================
Function: main
Purpose : not sure ;)
Input   : command line parameters
Output  : -
Comments: -
====================================================================== */
int main(void)
{
    // caught CTRL-C to do clean-up
    signal(SIGINT, sig_handler);

    printf("%s Starting\n", __BASEFILE__);

    /*
     * Initial vltData struct clearing
     */
    clearVltDataStruct(&vltData);

vltData.commands = 0x01;

      // Init GPIO bcm
    if (!bcm2835_init()) {
        fprintf( stderr, "bcm2835_init() Failed\n\n" );
        return 1;
    }

    // Show board config
    //printConfig(RF_LED_PIN);
    //printKeys();

    // Light off on board LED
    //pinMode(RF_LED_PIN, OUTPUT);
    //digitalWrite(RF_LED_PIN, HIGH);

    // LMIC init
    os_init();
    // Reset the MAC state. Session and pending data transfers will be discarded.
    LMIC_reset();
    LMIC_setAdrMode(LORAWAN_ADR_ON);
    // Try to join
    uint8_t appskey[sizeof(APPSKEY)];
    uint8_t nwkskey[sizeof(NWKSKEY)];
    memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
    memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
    LMIC_setSession (0x1, DEVADDR, nwkskey, appskey);

    // Select sub band, check this if correct for KPN?
    //LMIC_selectSubBand(1);

    // Disable link check validation, necessary???
    //LMIC_setLinkCheckMode(0);

    // TTN uses SF9 for its RX2 window.
    //LMIC.dn2Dr = DR_SF9;

    // Set data rate and transmit power for uplink (note: txpow seems to be ignored by the library)
    //LMIC_setDrTxpow(DR_SF7,14);

    // Start job
    do_send(&sendjob);

    while(!force_exit) {
      os_runloop_once();

      // We're on a multitasking OS let some time for others
      // Without this one CPU is 99% and with this one just 3%
      // On a Raspberry PI 3
      usleep(1000);
    }

    // We're here because we need to exit, do it clean

    // Light off on board LED
    //digitalWrite(RF_LED_PIN, LOW);

    // module CS line High
    digitalWrite(lmic_pins.nss, HIGH);
    printf( "\n%s, done my job!\n", __BASEFILE__ );
    bcm2835_close();
    return 0;
}

