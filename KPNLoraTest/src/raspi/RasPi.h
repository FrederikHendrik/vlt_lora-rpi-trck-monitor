// RasPi.h
//
// Routines for implementing RadioHead on Raspberry Pi
// using BCM2835 library for GPIO
// Contributed by Mike Poublon and used with permission

#ifndef RASPI_h
#define RASPI_h

#include <bcm2835.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include "hal/hal.h"
#include <lmic.h>


#include <assert.h>
#include <ifaddrs.h>
#include <netpacket/packet.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

typedef unsigned char byte;

#ifndef NULL
  #define NULL 0
#endif

#ifndef OUTPUT
  #define OUTPUT BCM2835_GPIO_FSEL_OUTP
#endif

#ifndef INPUT
  #define INPUT BCM2835_GPIO_FSEL_INPT
#endif

#ifndef NOT_A_PIN
  #define NOT_A_PIN 0xFF
#endif



#ifndef PROGMEM
#define PROGMEM
#endif

// No memcpy_P Raspberry PI
#ifndef memcpy_P
#define memcpy_P memcpy
#endif

// F() Macro
#define F(s)

#define random(x) (rand() % x)
#define delayMicroseconds(m) bcm2835_delayMicroseconds(m)

// We don't have IRQ on Raspberry PI
#define interrupts()   {}
#define noInterrupts() {}

#ifdef __cplusplus
extern "C"{
#endif
typedef unsigned char byte;

void RasPiSetup();

void pinMode(unsigned char pin, unsigned char mode);

void digitalWrite(unsigned char pin, unsigned char value);

unsigned char digitalRead(unsigned char pin) ;

unsigned long millis();

unsigned long  micros();

void delay (unsigned long delay);

//long random(long min, long max);

char *  getSystemTime(char * time_buff, int len);


void printbuffer(uint8_t buff[],int beg, int len);

class SPISettings
{
  public:
    SPISettings(uint16_t divider, uint8_t bitOrder, uint8_t dataMode) {
        init(divider, bitOrder, dataMode);
    }
    SPISettings() {
        init(BCM2835_SPI_CLOCK_DIVIDER_256, BCM2835_SPI_BIT_ORDER_MSBFIRST, BCM2835_SPI_MODE0);
    }
  private:
    void init(uint16_t divider, uint8_t bitOrder, uint8_t dataMode) {
      this->divider  = divider ;
      this->bitOrder = bitOrder;
      this->dataMode = dataMode;
    }

    uint16_t divider  ;
    uint8_t  bitOrder ;
    uint8_t  dataMode ;
  friend class SPIClass;
};


class SPIClass
{
  public:
    static byte transfer(byte _data);
    // SPI Configuration methods
    static void begin(); // Default
    static void begin(uint16_t, uint8_t, uint8_t);
    static void end();
    static void beginTransaction(SPISettings settings);
    static void endTransaction();
    static void setBitOrder(uint8_t);
    static void setDataMode(uint8_t);
    static void setClockDivider(uint16_t);
};

extern SPIClass SPI;

class SerialSimulator
{
  public:
    #define DEC 10
    #define HEX 16
    #define OCT 8
    #define BIN 2

    // TODO: move these from being inlined
    static void begin(int baud);
    static size_t println(const char* s);
    static size_t print(const char* s);
    static size_t print(unsigned int n, int base = DEC);
    static size_t print(char ch);
    static size_t println(char ch);
    static size_t print(unsigned char ch, int base = DEC);
    static size_t println(unsigned char ch, int base = DEC);
    static size_t write(char ch);
    static size_t write(unsigned char * s, size_t len);

};

extern SerialSimulator Serial;



#ifdef __cplusplus
}
#endif

#endif
