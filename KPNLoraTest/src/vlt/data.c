/*
 * data.c
 *
 *  Created on: May 4, 2016
 *      Author: merolewski
 */
//#include "main.h"
//#include "debug.h"
#include "data.h"
#include <time.h>
#include "lmic.h"
#include "kpnloratest.h"
#include <hal/hal.h>
//#include "adc.h"
//#include "hardware.h"
//#include "LTC3335.h"
//#include "rtc.h"

const char commandText[8][64] =
{   "0x01 = COMMAND_TIMESTAMP; Request for time sync.",
    "0x02 = COMMAND_OTHER; RFU."};

void clearVltDataStruct(struct vltData_s * vltData)
{
    vltData->frameVersion = 0;
    vltData->vBat.volt.val = 3920;
    vltData->vBat.percent.val = 6700;
    vltData->state = 0;
    vltData->commands = 0;
    vltData->timestamp.val = 0;
    vltData->sleepTime.time = 0;
    vltData->sleepTime.base = minutes;
    vltData->firstTimestamp = INIT_TIMESTAMP; //01.01.2000 - RTC at boot datetime
    vltData->beforeSyncTime = 0;
    vltData->useTimestampFlag = 1;
    vltData->useIRQFlag = 0;
    for (uint8_t i = 0; i < 32; i++)
    {
        vltData->requestList.counterUp[i] = (int32_t) -1;
        vltData->requestList.commands[i] = 0xFF;
    }
}

void getDataVltFrame(struct vltData_s *vltData)
{
    /*
     * State fulfilling
     */
     time_t t;


#if defined(IP54)
    if (HAL_GPIO_ReadPin(TAMPER_DETACH_GPIO_Port, TAMPER_DETACH_Pin))
    {
        vltData->state |= STATE_TAMPER_DETACH;
    }
    if (!HAL_GPIO_ReadPin(TAMPER_CASE_GPIO_Port, TAMPER_CASE_Pin))
    {
        vltData->state |= STATE_TAMPER_CASE;
    }
    if (!HAL_GPIO_ReadPin(CHG_PG_DETECT_GPIO_Port, CHG_PG_DETECT_Pin))
    {
        vltData->state |= STATE_POWER_GOOD;
    }
    if (!HAL_GPIO_ReadPin(CHG_CHG_DETECT_GPIO_Port, CHG_CHG_DETECT_Pin))
    {
        vltData->state |= STATE_POWER_CHARGING;
    }

    /*
     * not used
     */
//  if (HAL_GPIO_ReadPin(TAMPER_CASE_PHOTO_GPIO_Port, TAMPER_CASE_PHOTO_Pin))
//  {
//      vltData->state |= STATE_TAMPER_CASE;
//  }
    if (HAL_GPIO_ReadPin(TAMPER_CASE_SWITCH_GPIO_Port,
    TAMPER_CASE_SWITCH_Pin))
    {
        vltData->state |= STATE_TAMPER_CASE;
    }
#endif

#if DEBUG_UART
    debug_showStates(vltData->state);

    if (vltData->useTimestampFlag)
        debug_text("Using timestamp.");
    else
        debug_text("Waiting for timestamp.");
#endif

#if defined(IP54)

    uint32_t raw = 0;

    for (uint8_t i = 0; (raw == 0) && (i < 5); ++i)
    {
        HAL_ADC_Start(&hadc);
        raw = HAL_ADC_GetValue(&hadc);
        HAL_ADC_Stop(&hadc);
    }

    raw = (raw * 4190 / 2043);
    vltData->vBat.volt.val = (uint16_t) 0;
    vltData->vBat.percent.val = (uint16_t) 0;
    if (vltData->vBat.percent.val > 10000)
    vltData->vBat.percent.val = 10000;

#elif defined(IP67)

    uint32_t dc = 0;
    uint64_t percent = 0;

    LTC3335_Get_Discharged_Capacity(&dc, LTC3335_VBAT_TYP);

    percent = (BATT_CAP - dc) * 10000 / BATT_CAP;

    vltData->vBat.volt.val = LTC3335_VBAT_TYP;
    vltData->vBat.percent.val = (uint16_t) percent; //(vltData->vBat.volt.val - 3000) / 0.12;
    if (vltData->vBat.percent.val > 10000)
        vltData->vBat.percent.val = 10000;

#endif

#if DEBUG_UART
    debug_val("vBat Volt val: ", vltData->vBat.volt.val);
    debug_val("vBat Percent val: ", vltData->vBat.percent.val);
#endif

    if (vltData->useTimestampFlag)
    {
        t = time(NULL);
        vltData->timestamp.val = t;
    }
}

uint8_t prepareVltFrame(struct vltData_s * vltData)//, uint8_t * payloadLen)
{
    /*
     * command building
     */
    vltData->commands = 0x00;
    if (vltData->useTimestampFlag == 0 && !(LMIC.seqnoUp % 2))
        vltData->commands |= COMMAND_TIMESTAMP;

#if DEBUG_UART
    /*
     * show built commands
     */
    debug_showCommands(vltData->commands);
#endif

    /*
     * adding to requestList and deleting old ones
     */
    addPositionToRequestList(vltData, LMIC.seqnoUp);

    /*
     * state building
     */
    getDataVltFrame(vltData);

    uint8_t payloadLength = 0;

    LMIC.frame[payloadLength++] = vltData->frameVersion;
    for (uint8_t i = 0; i < 2; ++i)
        LMIC.frame[payloadLength++] = vltData->vBat.volt.bin[i];
    for (uint8_t i = 0; i < 2; ++i)
        LMIC.frame[payloadLength++] = vltData->vBat.percent.bin[i];
    LMIC.frame[payloadLength++] = vltData->state;
    LMIC.frame[payloadLength++] = vltData->commands;
    for (uint8_t i = 0; i < 4; ++i)
        LMIC.frame[payloadLength++] = vltData->timestamp.bin[i];

    //DO_DEVDB(vltData->frameVersion, frameVersion);
    //DO_DEVDB(vltData->vBat.volt.val, vBatV);
    //DO_DEVDB(vltData->vBat.percent.val, vBat%);
    //DO_DEVDB(vltData->state, state);
    //DO_DEVDB(vltData->commands, commands);
    //DO_DEVDB(vltData->timestamp.val, timestamp);

    return payloadLength;
}

void deletePositionFromRequestList(struct vltData_s * vltData,
        uint32_t downlinkAnswerValue)
{
    for (uint8_t i = 0; i < 32; ++i)
    {
        if (vltData->requestList.counterUp[i] == downlinkAnswerValue)
        {
            vltData->requestList.counterUp[i] = (int32_t) -1;
            vltData->requestList.commands[i] = 0xFF;
#if DEBUG_UART
            debug_text("Expected request found.");
            debug_val("On position: ", i);
            debug_val("Removed from requestList counter: ",
                    downlinkAnswerValue);
#endif
            break;
        }
    }
#if DEBUG_UART
    debug_text("Expected request not found.");
#endif
}

void deleteOldPositionFromRequestList(struct vltData_s * vltData,
        uint32_t currentMsgCounter, uint8_t oldMsgDelay)
{
    for (uint8_t i = 0; i < 32; ++i)
    {
        if (vltData->requestList.counterUp[i]
                < (currentMsgCounter - oldMsgDelay)
                && (vltData->requestList.counterUp[i] != -1))
        {
            vltData->requestList.counterUp[i] = (int32_t) -1;
            vltData->requestList.commands[i] = 0xFF;
#if DEBUG_UART
            debug_text((uint8_t*) "Old request found.");
            debug_val("On position: ", i);
            debug_val("Removed from requestList counter: ", currentMsgCounter);
#endif
        }
    }
}

void addPositionToRequestList(struct vltData_s * vltData,
        uint32_t uplinkRequestValue)
{
    if (vltData->commands != 0)
    {
        for (uint8_t i = 0; i < 32; ++i)
        {
            if (vltData->requestList.counterUp[i] == -1)
            {
                vltData->requestList.counterUp[i] = uplinkRequestValue;
                vltData->requestList.commands[i] = (int32_t) vltData->commands;
#if DEBUG_UART
                debug_val("Added to request list: ", LMIC.seqnoUp);
                debug_val("On position: ", i);
                debug_val("Current value in requestList: ",
                        vltData->requestList.counterUp[i]);
#endif
                break;
            }
        }
    }
}

void executeCommands(uint8_t commandByte, uint8_t * dataPointer,
        struct vltData_s * vltData)
{

    uint8_t commandPosition = 0;
     time_t t;
    /*
     * Timestamp
     */
    if (commandByte & COMMAND_TIMESTAMP)
    {
        for (uint8_t i = 0; i < 4; ++i)
            vltData->timestamp.bin[i] = dataPointer[i];

        /*
         * LTC3335 firstTimestamp offset
         */
        if (vltData->useTimestampFlag == 0)
        {
            vltData->firstTimestamp = vltData->timestamp.val;
                    t = time(NULL);

            vltData->beforeSyncTime = t;
        }

        //HAL_RTC_setRTCFromEpoch(t, vltData->timestamp.val);

        vltData->useTimestampFlag = 1;

        commandPosition += 4;
    }

    /*
     * Other message
     */
    if (commandByte & COMMAND_OTHER)
    {
        union uint16_u temp;
        temp.val = 0;

        for (uint8_t i = 0; i < 2; ++i)
            temp.bin[i] = dataPointer[i + commandPosition];

        commandPosition += 2;
    }
}

#ifdef __cplusplus
extern "C"{
#endif

void debug_text(const char* str)
{

    while (*str)
    {
        printf(str++);
}
    printf("\n");


}

void debug_charc(const char *c)
{

    printf( c);

}

void debug_chard(char c)
{

    printf("%c", c);

}

void debug_char(char *c)
{

    printf( c);

}

void debug_str(const char* str)
{

    while (*str)
    {
        debug_charc(str++);
    }

}

void debug_showCommands(uint8_t commandByte)
{

    debug_text("Device commands:");
    if (commandByte != 0)
    {
        for (uint8_t i = 0, bit = 1; i < 8; ++i, bit <<= 1)
        {
            if (commandByte & bit)
            debug_text(commandText[i]);
        }
    }
    else
    {
        debug_text("0x00");
    }

}


void debug_hex(u1_t b)
{

    debug_chard("0123456789ABCDEF"[b >> 4]);
    debug_chard("0123456789ABCDEF"[b & 0xF]);

}

void debug_hexString(uint8_t * buffer, uint8_t length)
{

    for (uint8_t i = 0; i < length; ++i)
    debug_hex(buffer[i]);
    debug_text("");
    char buff[32] = "";
    snprintf((char *)buffer, 32, "0x");
    debug_str((char*)buffer);


}
void debug_uint(u4_t v)
{

    for (s1_t n = 24; n >= 0; n -= 8)
    {
        debug_hex(v >> n);
    }

}

void debug_num(uint32_t val)
{

    char buffer[32] = "";
    snprintf(buffer, 32, "%d", val);
    debug_str(buffer);

}

void debug_buf (const u1_t* buf, int len) {
    while(len--) {
        debug_hex(*buf++);
        debug_chard(' ');
    }
    debug_chard('\r');
    debug_chard('\n');
}

void debug_val(const char* label, u4_t val)
{

    debug_str(label);
    debug_uint(val);
    debug_str(" = ");
    debug_num(val);
    debug_chard('\r');
    debug_chard('\n');

}
#ifdef __cplusplus
} // extern "C"
#endif
