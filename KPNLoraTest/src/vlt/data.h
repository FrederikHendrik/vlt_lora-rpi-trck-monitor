/*
 * data.h
 *
 *  Created on: May 4, 2016
 *      Author: merolewski
 */

//! \file

#ifndef APPLICATION_USER_INC_DATA_H_
#define APPLICATION_USER_INC_DATA_H_

#include <stdint.h>
#include <hal/hal.h>
#include <stdio.h>
#include <string.h>

#ifdef __cplusplus
extern "C"{
#endif
#define BATT_CAP (uint64_t)61200000 //in mAs

#include "enum.h"

/**
 * @defgroup MYDATA
 * @brief Custom types, structures and functions.
 */

 #define INIT_TIMESTAMP 946684800

/**
 * @addtogroup MYDATA
 * @{
 */

/**
 * @union uint16_u
 */
union uint16_u
{
    uint16_t val; ///< `uint16_t` value
    uint8_t bin[2]; ///< binary (`uint8_t`) representation
};

/**
 * @union uint32_u
 */
union uint32_u
{
    uint32_t val; ///< `uint32_t` value
    uint8_t bin[4]; ///< binary (`uint8_t`) representation
};

/**
 * @struct vBat_s
 * @brief Structure containing battery voltage
 */
struct vBat_s
{
    union uint16_u volt; ///< Value in `mV`
    union uint16_u percent; ///< Percent value (`3000 mV` = `0%`; `4200 mV` = `100%`)
};

/**
 * @struct requestList_s
 * @brief Structure containing requests list.
 */
struct requestList_s
{
    int32_t counterUp[32]; ///< counter number
    uint8_t commands[32]; ///< command
};

/**
 * @struct sleepTime_s
 * @brief Structure containing time and basetime used in setting device in stop mode.
 */
struct sleepTime_s
{
    uint32_t time; ///< sleep time
    enum timeBase_e base; ///< time base
};

/**
 * @struct vltData_s
 * @brief Structure containing all data.
 */
struct vltData_s
{
    uint8_t frameVersion; ///< frame vesion info
    struct vBat_s vBat; ///< battery info structure
    struct vBat_s vBatOld; ///< old battery info structure
    uint8_t state; ///< device state
    uint8_t commands; ///< commands
    union uint32_u timestamp; ///< timestamp
    struct requestList_s requestList; ///< request list
    struct sleepTime_s sleepTime; ///< sleep time structure
    uint32_t firstTimestamp;
    uint32_t beforeSyncTime;
    uint8_t useTimestampFlag; ///< use timestamp decision flag
    uint8_t useIRQFlag; ///< enable Tamper Case GPIOs' IRQ
};

/**
 * @struct resetTask_s
 * @brief Structure containing scheduler task "watchdog"
 */
struct resetTask_s
{
    uint32_t time;
    uint8_t flag;
};

/**
 * @brief Clear vltData struct with zeros and -1.
 * @param vltData pointer to vltData struct
 */
void clearVltDataStruct(struct vltData_s * vltData);

/**
 * @brief Get data needed to prepare LoRa frame.
 * @param vltData pointer to vltData struct
 */
void getDataVltFrame(struct vltData_s *vltData);

/**
 * @brief Prepare LoRa frame to send.
 * @param vltData pointer to vltData struct
 */
uint8_t prepareVltFrame(struct vltData_s * vltData);

/**
 * @brief Add position to request list.
 * @param vltData pointer to vltData struct
 * @param uplinkRequestValue value to be written on first available place
 */
void addPositionToRequestList(struct vltData_s * vltData,
        uint32_t uplinkRequestValue);

/**
 * @brief Delete position from request list.
 * @param vltData pointer to vltData struct
 * @param downlinkAnswerValue value to be found and deleted
 */
void deletePositionFromRequestList(struct vltData_s * vltData,
        uint32_t downlinkAnswerValue);

/**
 * @brief Delete old positions from request list.
 * @param vltData pointer to vltData struct
 * @param currentMsgCounter current message sequence number
 * @param oldMsgDelay `currentMsgCounter - oldMsgDelay = MsgNumber to be deleted`
 */
void deleteOldPositionFromRequestList(struct vltData_s * vltData,
        uint32_t currentMsgCounter, uint8_t oldMsgDelay);

/**
 * @brief Execute download commands.
 * @param commandByte Byte containing commands' bits
 * @param dataPointer pointer to download data
 * @param vltData pointer to vltData struct
 */
void executeCommands(uint8_t commandByte, uint8_t * dataPointer,
        struct vltData_s * vltData);

void debug_text(const char* str);
void debug_chard(char c);
void debug_charc(const char *c);
void debug_char(char *c);
void debug_showCommands(uint8_t commandByte);

void debug_hexString(uint8_t * buffer, uint8_t length);
void debug_uint(u4_t v);
void debug_hex(u1_t b);
void debug_val(const char* label, u4_t val);
void debug_num(uint32_t val);
void debug_buf (const u1_t* buf, int len);
/**
 * @}
 */

#ifdef __cplusplus
} // extern "C"
#endif
#endif /* APPLICATION_USER_INC_DATA_H_ */
