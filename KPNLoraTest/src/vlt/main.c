**
 ******************************************************************************
 * File Name          : main.c
 * Description        : Main program body
 ******************************************************************************
 *
 * COPYRIGHT(c) 2016 STMicroelectronics
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx_hal.h"
#include "adc.h"
#include "i2c.h"
#include "iwdg.h"
#include "rtc.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "usb.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "oslmic.h"
#include "lmic.h"
#include "debug.h"
#include "stdbool.h"
#include "led.h"
#include "main.h"
#include "data.h"
#include "keys.h"
#include "tasks.h"
#include "LTC3335.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
/*!
 * Defines the network ID when using personalization activation procedure
 */
#define LORAWAN_NET_ID                              ( u4_t )0x00000000

/*!
 * LoRaWAN Adaptative Data Rate
 */
#define LORAWAN_ADR_ON                              1
#define LORAWAN_ADR_OFF                             0

/*!
 * LoRaWAN confirmed messages
 */
#define LORAWAN_CONFIRMED_MSG_ON                    1
#define LORAWAN_CONFIRMED_MSG_OFF                   0

osjob_t initJob, sendVltFrameJob, allowStopJob;
struct vltData_s vltData;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
//////////////////////////////////////////////////
// Utility functions
//////////////////////////////////////////////////
/*!
 * \brief Computes a random number between min and max
 *
 * \param [IN] min range minimum value
 * \param [IN] max range maximum value
 * \retval random random value in range min..max
 */
/*
 * unused
 */
s4_t randr(s4_t min, s4_t max)
{
	return (s4_t) rand() % (max - min + 1) + min;
}
//////////////////////////////////////////////////
// APPLICATION CALLBACKS
//////////////////////////////////////////////////
// provide application router ID (8 bytes, LSBF)
void os_getArtEui(u1_t *buf)
{
	memcpy(buf, APPEUI, 8);
}

// provide device ID (8 bytes, LSBF)
void os_getDevEui(u1_t *buf)
{
	memcpy(buf, DEVEUI, 8);
}

// provide device key (16 bytes)
void os_getDevKey(u1_t *buf)
{
	memcpy(buf, DEVKEY, 16);
}

static void processRxFrame(void)
{
	uint8_t downlinkPayload[64] =
	{ 0 };

#if DEBUG_UART
	debug_text("DOWNLINK OCCURRED:");
	debug_str("Entire frame: ");
	debug_hexString(LMIC.frame, 32);
#endif

	for (uint8_t j = 0, i = LMIC.dataBeg; i < LMIC.dataBeg + LMIC.dataLen;
			++i, ++j)
	{
		downlinkPayload[j] = (uint8_t) LMIC.frame[i];
	}

#if DEBUG_UART
	debug_str("PAYLOAD: ");
	debug_hexString(downlinkPayload, LMIC.dataLen);
	debug_val("Frame Version: ", downlinkPayload[0]);
#endif

	union uint16_u downlinkAnswer;
	downlinkAnswer.val = 0;
	for (uint8_t i = 0; i < 2; ++i)
		downlinkAnswer.bin[i] = downlinkPayload[i + 1];

#if DEBUG_UART
	debug_val("Answer for uplink message no.: ", downlinkAnswer.val);
	debug_text("Downlink commands:");
	debug_showCommands(downlinkPayload[3]);
#endif

	executeCommands(downlinkPayload[3], &downlinkPayload[4], &vltData);
	deletePositionFromRequestList(&vltData, downlinkAnswer.val);

#if DEBUG_LED
#if defined(IP54)
	ledBlink(LED_GREEN_RED,50,5);
#elif defined(IP67)
	ledBlink(LED_RED_YELLOW, 50, 5);
#endif
#endif
}

void sendVltFrameCB(osjob_t *j)
{
	HAL_IRQ_disableTamper();
#if DEBUG_UART
	debug_softVer();
	debug_hardVer();
	debug_lora();
#endif
#if DEBUG_LED
#if defined(IP54)
	ledBlink(LED_GREEN_RED, 50, 1);
#elif defined(IP67)
	ledBlink(LED_RED_YELLOW, 50, 1);
#endif
#endif

	uint8_t payloadLen = prepareVltFrame(&vltData);
#if DEBUG_UART
	debug_str("UPLINK: ");
	debug_hexString(LMIC.frame, payloadLen);
#endif

	/*
	 * after-send state clear
	 */
	vltData.state = 0x00;

#if DEBUG_UART
	debug_str("LMIC_setTxData2 at: ");
	debug_datetime(2);
#endif

	/*
	 * Send packet and save its time
	 */
	LMIC_setTxData2(LORAWAN_APP_PORT, LMIC.frame, payloadLen,
	LORAWAN_CONFIRMED_MSG_ON); //XXX should be ON
}

// Initialization job
static void initCB(osjob_t* j)
{
	/*
	 * LMIC resetting and session initialization with Adaptive Datarate enabled
	 */
	LMIC_reset();
	LMIC_setAdrMode(LORAWAN_ADR_ON);
	LMIC_setSession(LORAWAN_NET_ID, LORAWAN_DEV_ADDR, NWKSKEY, ARTSKEY);

	/*
	 * Show current device UUID
	 */
#if DEBUG_KEYS
	DO_DEVDB(LORAWAN_DEV_ADDR, LORAWAN_DEV_ADDR);
	DO_DEVDB(LORAWAN_APP_PORT, LORAWAN_APP_PORT);
#endif

	/*
	 * Initial vltData struct clearing
	 */
	clearVltDataStruct(&vltData);

	/*
	 * Set timestamp request on boot
	 */
	vltData.commands = 0x01;

	/*
	 * Don't add timestamp to payload after boot
	 */
	vltData.useTimestampFlag = 0;

#if DEBUG_TEST
	/*
	 * TESTING
	 */
	dataTestLoop(1000);
#endif

	/*
	 * send first frame in 5 seconds
	 */
	vltData.sleepTime.time = 5; //XXX should be 5
	vltData.sleepTime.base = seconds; //XXX should be seconds
	HAL_RTC_setWakeUp(&hrtc, &vltData.sleepTime);
}
/* USER CODE END 0 */

int main(void)
{

	/* USER CODE BEGIN 1 */
	/*
	 * Initialize runtime environment
	 */
	os_init();

	/*
	 * Initialize debug library / print debug title
	 */
#if DEBUG_UART
	debug_init();
#endif

	/*
	 * prepare MCU for stop mode
	 * Wait state HAS to be set to 0:
	 * FLASH_LATENCY_0
	 * otherwise resetting occurs
	 */
	SystemPower_Config();

	/*
	 * Setup initial job
	 */
	os_setCallback(&initJob, initCB);

	/*
	 * Execute scheduled jobs and events
	 */
	os_runloop();

	/*
	 * Never reached
	 */
	return 0;
	/* USER CODE END 1 */

	/* MCU Configuration----------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* Configure the system clock */
	SystemClock_Config();

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_ADC_Init();
	MX_I2C1_Init();
	MX_SPI1_Init();
	MX_USART1_UART_Init();
	MX_USART2_UART_Init();
	MX_IWDG_Init();
	MX_RTC_Init();
	MX_USB_PCD_Init();

	/* USER CODE BEGIN 2 */

	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1)
	{
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */

	}
	/* USER CODE END 3 */

}

/** System Clock Configuration
 */
void SystemClock_Config(void)
{

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_PeriphCLKInitTypeDef PeriphClkInit;

	__HAL_RCC_PWR_CLK_ENABLE()
	;

	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI
			| RCC_OSCILLATORTYPE_LSI | RCC_OSCILLATORTYPE_LSE;
	RCC_OscInitStruct.LSEState = RCC_LSE_ON;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = 16;
	RCC_OscInitStruct.LSIState = RCC_LSI_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
	RCC_OscInitStruct.PLL.PLLDIV = RCC_PLL_DIV3;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}

	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
	{
		Error_Handler();
	}

	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC;
	PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
	{
		Error_Handler();
	}

	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */
void SystemPower_Config(void)
{
	/* Note: For STM32L1, to enable low power sleep mode,
	 * the system frequency should not exceed f_MSI range1.
	 * Set MSI range to 0
	 */
	__HAL_RCC_MSI_RANGE_CONFIG(RCC_MSIRANGE_0);

	/*
	 * Enable Ultra low power mode
	 */
	HAL_PWREx_EnableUltraLowPower();

	/*
	 * Enable the fast wake up from Ultra low power mode
	 */
	HAL_PWREx_EnableFastWakeUp();

	/*
	 * Disable all used wakeup sources
	 */
	HAL_RTCEx_DeactivateWakeUpTimer(&hrtc);
}

//////////////////////////////////////////////////
// LMIC EVENT CALLBACK
//////////////////////////////////////////////////
void onEvent(ev_t ev)
{
	bool txOn = false;
#if DEBUG_UART
		static uint8_t receivedCounter = 0, notHandledEvCnt = 0;
	debug_event(ev);
#endif

	switch (ev)
	{
	/*
	 * Reinit session after reset
	 */
	case EV_RESET:
		initCB(&initJob);
		break;

		/*
		 * If link is dead go to sleep, try again next time
		 */
	case EV_LINK_DEAD:
#if DEBUG_LED
#if defined(IP54)
		ledBlink(LED_RED_YELLOW, 1000, 1);
#elif defined(IP67)
		ledBlink(LED_GREEN_RED, 1000, 1);
#endif
#endif
		break;

	case EV_LINK_ALIVE:
#if DEBUG_LED
#if defined(IP54)
		ledBlink(LED_GREEN_RED, 1000, 1);
#elif defined(IP67)
		ledBlink(LED_RED_YELLOW, 1000, 1);
#endif
#endif
		break;

	case EV_LOWER_DR:
#if DEBUG_LED
		ledBlink(LED_BOTH, 50, 2);
#endif
		break;

	case EV_NO_RX:
#if DEBUG_LED
#if defined(IP54)
		ledBlink(LED_RED_YELLOW, 1000, 1);
#elif defined(IP67)
		ledBlink(LED_GREEN_RED, 1000, 1);
#endif
#endif
		break;

		/*
		 * Scheduled data sent (optionally data received)
		 */
	case EV_TXCOMPLETE:
#if DEBUG_UART
		debug_val("Received counter: ", ++receivedCounter);
		debug_val("Datarate = ", LMIC.datarate);
#endif
		/*
		 * Check if we have a downlink on either RX_1 or RX_2 windows
		 */
		if ((LMIC.txrxFlags & (TXRX_DNW1 | TXRX_DNW2)) != 0)
		{
#if DEBUG_LED
#if defined(IP54)
			ledBlink(LED_GREEN_RED, 50, 2);
#elif defined(IP67)
			ledBlink(LED_RED_YELLOW, 50, 2);
#endif
#endif

			if (LMIC.dataLen != 0)
			{
				/*
				 * data received in RX slot after TX
				 */
				processRxFrame();
			}
		}
		txOn = true;

#if DEBUG_UART
		debug_str("EV_TXCOMPLETE at: ");
		debug_datetime(2);

		debug_val("LMIC.txCnt: ", LMIC.txCnt);

		debug_val("LMIC.adrAckReq: ", LMIC.adrAckReq);

#endif

		break;
	default:
		/*
		 * XXX check it out
		 */
#if DEBUG_UART
		debug_val("Not handled event No. ", ++notHandledEvCnt);
#endif
		break;
	}
	if (txOn == true)
	{
#if DEBUG_LED
		/*
		 * put LEDs in high state to turn them off
		 */
		ledOff(LED_BOTH);
#endif
#if DEBUG_SHORTLOOP
		//XXX short loop settings
		vltData.sleepTime.time = TIME;
		vltData.sleepTime.base = BASE;
#else
		/*
		 * Set RTC internal wake up and allows to go to stop mode
		 */
		if (vltData.useTimestampFlag)
		{
			vltData.sleepTime.time = TIME; //XXX should be 1 or 15
			vltData.sleepTime.base = BASE;//XXX should be hour or minutes
		}
		else
		{
			vltData.sleepTime.time = 15; //XXX should be 15 min
			vltData.sleepTime.base = minutes;//XXX should be minutes
		}
#endif
		HAL_RTC_setWakeUp(&hrtc, &vltData.sleepTime);
	}
}
/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  None
 * @retval None
 */
void Error_Handler(void)
{
	/* USER CODE BEGIN Error_Handler */
	errorHandler();
	/* User can add his own implementation to report the HAL error return state */
	while (1)
	{
	}
	/* USER CODE END Error_Handler */
}

#ifdef USE_FULL_ASSERT

/**
 * @brief Reports the name of the source file and the source line number
 * where the assert_param error has occurred.
 * @param file: pointer to the source file name
 * @param line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
	 ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */

}

#endif

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
