/**
 ******************************************************************************
 * File Name          : RTC.c
 * Description        : This file provides code for the configuration
 *                      of the RTC instances.
 ******************************************************************************
 *
 * COPYRIGHT(c) 2016 STMicroelectronics
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "rtc.h"

/* USER CODE BEGIN 0 */
#include "lmic.h"
#include "debug.h"
#include "oslmic.h"
static uint32_t startupTime = 0;
/* USER CODE END 0 */

RTC_HandleTypeDef hrtc;

/* RTC init function */
void MX_RTC_Init(void)
{
    RTC_TimeTypeDef sTime;
    RTC_DateTypeDef sDate;

    /**Initialize RTC and set the Time and Date
     */
    hrtc.Instance = RTC;
    hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
    hrtc.Init.AsynchPrediv = 127;
    hrtc.Init.SynchPrediv = 255;
    hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
    hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
    hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
    if (HAL_RTC_Init(&hrtc) != HAL_OK)
    {
        Error_Handler();
    }

    sTime.Hours = 0;
    sTime.Minutes = 0;
    sTime.Seconds = 0;
    sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
    sTime.StoreOperation = RTC_STOREOPERATION_RESET;
    if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN) != HAL_OK)
    {
        Error_Handler();
    }

    sDate.WeekDay = RTC_WEEKDAY_MONDAY;
    sDate.Month = RTC_MONTH_JANUARY;
    sDate.Date = 1;
    sDate.Year = 0;

    if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BIN) != HAL_OK)
    {
        Error_Handler();
    }

    /**Enable the WakeUp
     */
    if (HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, 65535,
    RTC_WAKEUPCLOCK_CK_SPRE_16BITS) != HAL_OK)
    {
        Error_Handler();
    }

    startupTime = HAL_RTC_getEpochFromRTC(&hrtc);
}

void HAL_RTC_MspInit(RTC_HandleTypeDef* rtcHandle)
{

    if (rtcHandle->Instance == RTC)
    {
        /* USER CODE BEGIN RTC_MspInit 0 */

        /* USER CODE END RTC_MspInit 0 */
        /* Peripheral clock enable */
        __HAL_RCC_RTC_ENABLE();

        /* Peripheral interrupt init */
        HAL_NVIC_SetPriority(RTC_WKUP_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(RTC_WKUP_IRQn);
        /* USER CODE BEGIN RTC_MspInit 1 */

        /* USER CODE END RTC_MspInit 1 */
    }
}

void HAL_RTC_MspDeInit(RTC_HandleTypeDef* rtcHandle)
{

    if (rtcHandle->Instance == RTC)
    {
        /* USER CODE BEGIN RTC_MspDeInit 0 */

        /* USER CODE END RTC_MspDeInit 0 */
        /* Peripheral clock disable */
        __HAL_RCC_RTC_DISABLE();

        /* Peripheral interrupt Deinit*/
        HAL_NVIC_DisableIRQ(RTC_WKUP_IRQn);

    }
    /* USER CODE BEGIN RTC_MspDeInit 1 */

    /* USER CODE END RTC_MspDeInit 1 */
}

/* USER CODE BEGIN 1 */
void HAL_RTC_setDateAndTime(RTC_HandleTypeDef* hrtc, RTC_TimeTypeDef *sTime,
        RTC_DateTypeDef *sDate)
{
    HAL_RTC_SetTime(hrtc, sTime, RTC_FORMAT_BIN);
    HAL_RTC_SetDate(hrtc, sDate, RTC_FORMAT_BIN);
}

void HAL_RTC_getDateAndTime(RTC_HandleTypeDef* hrtc, RTC_TimeTypeDef *sTime,
        RTC_DateTypeDef *sDate)
{
    HAL_RTC_GetTime(hrtc, sTime, RTC_FORMAT_BIN);
    HAL_RTC_GetDate(hrtc, sDate, RTC_FORMAT_BIN);
}

void HAL_RTC_setRTCFromEpoch(RTC_HandleTypeDef* hrtc, uint32_t epoch)
{
    uint32_t previousEpoch = HAL_RTC_getEpochFromRTC(&hrtc);
    RTC_TimeTypeDef sTime;
    RTC_DateTypeDef sDate;
    HAL_RTC_setTypeDefs(epoch, &sTime, &sDate);
    HAL_RTC_setDateAndTime(hrtc, &sTime, &sDate);

    //correct startup time after updating the RTC
    startupTime += epoch - previousEpoch;
}

uint32_t HAL_RTC_getEpochFromRTC(RTC_HandleTypeDef* hrtc)
{
    RTC_TimeTypeDef sTime;
    RTC_DateTypeDef sDate;
    HAL_RTC_getDateAndTime(hrtc, &sTime, &sDate);
    return HAL_RTC_getEpoch(&sTime, &sDate);
}

// Convert Date/Time structures to epoch time
uint32_t HAL_RTC_getEpoch(RTC_TimeTypeDef *sTime, RTC_DateTypeDef *sDate)
{
    uint8_t a;
    uint16_t y;
    uint8_t m;
    uint32_t JDN;

    // These hardcore math's are taken from http://en.wikipedia.org/wiki/Julian_day

    // Calculate some coefficients
    a = (14 - sDate->Month) / 12;
    y = (sDate->Year + 2000) + 4800 - a; // years since 1 March, 4801 BC
    m = sDate->Month + (12 * a) - 3; // since 1 March, 4801 BC

    // Gregorian calendar date compute
    JDN = sDate->Date;
    JDN += (153 * m + 2) / 5;
    JDN += 365 * y;
    JDN += y / 4;
    JDN += -y / 100;
    JDN += y / 400;
    JDN = JDN - 32045;
    JDN = JDN - JULIAN_DATE_BASE;    // Calculate from base date
    JDN *= 86400;                     // Days to seconds
    JDN += sTime->Hours * 3600;    // ... and today seconds
    JDN += sTime->Minutes * 60;
    JDN += sTime->Seconds;

    return JDN;
}

// Convert epoch time to Date/Time structures
void HAL_RTC_setTypeDefs(uint32_t epoch, RTC_TimeTypeDef *sTime,
        RTC_DateTypeDef *sDate)
{
    uint32_t tm;
    //uint32_t t1;
    uint32_t a;
    uint32_t b;
    uint32_t c;
    uint32_t d;
    uint32_t e;
    uint32_t m;
    int16_t year = 0;
    int16_t month = 0;
    int16_t dow = 0;
    int16_t mday = 0;
    int16_t hour = 0;
    int16_t min = 0;
    int16_t sec = 0;
    uint64_t JD = 0;
    uint64_t JDN = 0;

    // These hardcore math's are taken from http://en.wikipedia.org/wiki/Julian_day

    JD = ((epoch + 43200) / (86400 >> 1)) + (2440587 << 1) + 1;
    JDN = JD >> 1;

//    tm = epoch; t1 = tm / 60; sec  = tm - (t1 * 60);
//    tm = t1;    t1 = tm / 60; min  = tm - (t1 * 60);
//    tm = t1;    t1 = tm / 24; hour = tm - (t1 * 24);

    tm = epoch;
    sec = epoch % 60;
    min = (tm - sec) / 60 % 60;
    hour = (tm - min - sec) / 3600 % 24;

    dow = JDN % 7;
    a = JDN + 32044;
    b = ((4 * a) + 3) / 146097;
    c = a - ((146097 * b) / 4);
    d = ((4 * c) + 3) / 1461;
    e = c - ((1461 * d) / 4);
    m = ((5 * e) + 2) / 153;
    mday = e - (((153 * m) + 2) / 5) + 1;
    month = m + 3 - (12 * (m / 10));
    year = (100 * b) + d - 4800 + (m / 10);

    sDate->Year = year - 2000;
    sDate->Month = month;
    sDate->Date = mday;
    sDate->WeekDay = dow;
    sTime->Hours = hour;
    sTime->Minutes = min;
    sTime->Seconds = sec;
    sTime->DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
    sTime->StoreOperation = RTC_STOREOPERATION_RESET;
}

void HAL_RTC_setWakeUp(RTC_HandleTypeDef *hrtc, struct sleepTime_s *sleepTime)
{
#if DEBUG_UART
    debug_text("RTC Wake-Up used.");

    /*
     * set sleepTime in vltData depending on switch-case
     */
    const char sec[] = "seconds";
    const char min[] = "minutes";
    const char hrs[] = "hours";
    char *text;
    text = hrs;
#endif

    uint32_t thisTime = sleepTime->time;
    switch (sleepTime->base)
    {
    case seconds:
//      time;
#if DEBUG_UART
        text = sec;
#endif
        break;
    case minutes:
        thisTime *= 60;
#if DEBUG_UART
        text = min;
#endif
        break;
    case hours:
        thisTime *= 3600;
#if DEBUG_UART
        text = hrs;
#endif
        break;
    }

    /*
     * counter can't be greater than 0xFFFF
     */
    SANITY_CHECK(thisTime <= 0xFFFF, "WRONG RTC COUNTER");

#if DEBUG_UART
    uint8_t debugBuffer[32] = "";
    snprintf(debugBuffer, 32, "Awake in %u %s.", sleepTime->time, text);
    debug_text(debugBuffer);
#endif

    /*
     * set wake-up timer on RTC
     */
    HAL_RTCEx_SetWakeUpTimer_IT(hrtc, thisTime,
    RTC_WAKEUPCLOCK_CK_SPRE_16BITS);

    hal_stopmode();
}

uint32_t HAL_RTC_GetUptime()
{
    return HAL_RTC_getEpochFromRTC(&hrtc) - startupTime;
}

/* USER CODE END 1 */

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
