/*
 * keys.h
 *
 *  Created on: May 17, 2016
 *      Author: merolewski
 */

#ifndef APPLICATION_USER_INC_KEYS_H_
#define APPLICATION_USER_INC_KEYS_H_

extern const u1_t APPEUI[8];
extern const u1_t DEVEUI[8];
extern const u1_t DEVKEY[16];
extern u1_t NWKSKEY[];
extern u1_t ARTSKEY[];

/*
 * XXX Define VLT number
 */
#define VLTNEW10

#if defined(VLTOLD1)
#define LORAWAN_DEV_ADDR                            ( u4_t )0x0000115E
#define LORAWAN_APP_PORT                            2
#define VLT_ID										"AA0001"
#elif defined(VLTNEW1)
#define LORAWAN_DEV_ADDR                            ( u4_t )0x1400C649
#define LORAWAN_APP_PORT                            1
#define VLT_ID										"AA0001"
#elif defined(VLTOLD2)
#define LORAWAN_DEV_ADDR                            ( u4_t )0x0000153C
#define LORAWAN_APP_PORT                            2
#define VLT_ID										"AA0002"
#elif defined(VLTNEW2)
#define LORAWAN_DEV_ADDR                            ( u4_t )0x14001161
#define LORAWAN_APP_PORT                            2
#define VLT_ID										"AA0002"
#elif defined(VLTOLD3)
#define LORAWAN_DEV_ADDR                            ( u4_t )0x0000153D
#define LORAWAN_APP_PORT                            2
#define VLT_ID										"AB0001"
#elif defined(VLTNEW3)
#define LORAWAN_DEV_ADDR                            ( u4_t )0x140011AA
#define LORAWAN_APP_PORT                            3
#define VLT_ID										"AB0001"
#elif defined(VLTOLD4)
#define LORAWAN_DEV_ADDR                            ( u4_t )0x0000153E
#define LORAWAN_APP_PORT                            2
#define VLT_ID										"AB0002"
#elif defined(VLTNEW4)
#define LORAWAN_DEV_ADDR                            ( u4_t )0x14005EE8
#define LORAWAN_APP_PORT                            4
#define VLT_ID										"AB0002"
#elif defined(VLTOLD5)
#define LORAWAN_DEV_ADDR                            ( u4_t )0x0000153F
#define LORAWAN_APP_PORT                            2
#define VLT_ID										"AC0001"
#elif defined(VLTNEW5)
#define LORAWAN_DEV_ADDR                            ( u4_t )0x1400C631
#define LORAWAN_APP_PORT                            5
#define VLT_ID										"AC0001"
#elif defined(VLTDEV)
#define LORAWAN_DEV_ADDR                            ( u4_t )0x0000115D
#define LORAWAN_APP_PORT                            1
#define VLT_ID										"AD0001 or AD0002"
#elif defined(VLTNEW6)
#define LORAWAN_DEV_ADDR                            ( u4_t )0x1400B139
#define LORAWAN_APP_PORT                            6
#define VLT_ID										"BB0001"
#elif defined(VLTNEW7)
#define LORAWAN_DEV_ADDR                            ( u4_t )0x1400B118
#define LORAWAN_APP_PORT                            7
#define VLT_ID										"BB0002"
#elif defined(VLTNEW8)
#define LORAWAN_DEV_ADDR                            ( u4_t )0x14002337
#define LORAWAN_APP_PORT                            8
#define VLT_ID										"BB0003"
#elif defined(VLTNEW9)
#define LORAWAN_DEV_ADDR                            ( u4_t )0x1400B73D
#define LORAWAN_APP_PORT                            9
#define VLT_ID										"BB0004"
#elif defined(VLTNEW10)
#define LORAWAN_DEV_ADDR                            ( u4_t )0x14007B4B
#define LORAWAN_APP_PORT                            10
#define VLT_ID										"BB0005"
#endif

#endif /* APPLICATION_USER_INC_KEYS_H_ */
