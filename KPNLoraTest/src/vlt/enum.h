/*
 * enum.h
 *
 *  Created on: Apr 27, 2016
 *      Author: merolewski
 */

#ifndef APPLICATION_USER_INC_ENUM_H_
#define APPLICATION_USER_INC_ENUM_H_

/**
 * @enum timeBase_e
 * @brief Timebase enum used in setting device in stop mode.
 */
enum timeBase_e
{
	seconds, minutes, hours
};

#endif /* APPLICATION_USER_INC_ENUM_H_ */
